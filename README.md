## Simple TODO LIST (REST api)

*   Jedná se jednoduchý todo list. 
*   Symfony server a databáze v dockeru 
*   Security je řešeno Bearear tokenem 
* Aplikace se testuje pomocí POSTMANu

Předdefinovaný user (ve fixtures): 

*   username: pepa@novak.cz
*   password: 123456

Seznam všech rout a příklad requestů jsou v souboru **Todo.postman\_collection.json** , který stačí jenom naimportovat do Postmanu

### Spuštění projektu: 

1.  composer install
2.  docker-compose up --build
3.  php bin/console d:m:m
4.  php bin/console d:f:l --append
5.  symfony serve:start
