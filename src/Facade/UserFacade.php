<?php

declare(strict_types=1);

namespace App\Facade;

use App\DTO\ErasedUserDto;
use App\DTO\JwtPayload;
use App\Entity\User;
use App\Security\Provider\JwtProvider;

class UserFacade
{
    public function __construct(
        private readonly JwtProvider $jwtProvider,
    ) {
    }

    public function getErasedUser(User $user): ErasedUserDto
    {
        $token = $this->jwtProvider->encode(
            new JwtPayload(
                $user->getUserIdentifier(),
                (new \DateTime('+30 days'))->getTimestamp(),
            ),
        );

        return new ErasedUserDto(
            $user->getName(),
            $user->getUserIdentifier(),
            $token
        );
    }
}
