<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\Exception\JsonException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFactory
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $em,

    ) {
    }

    public function create(string $name, string $username, string $plainPassword): User
    {
        if ($this->userRepository->findOneBy(['username' => $username]) !== null) {
            throw new JsonException('Not unique username', 400);
        }

        $user = new User(
            $name,
            $username,
        );

        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $plainPassword)
        );

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
