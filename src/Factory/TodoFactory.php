<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Todo;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class TodoFactory
{
    public function __construct(
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function create(string $title, string $description, User $user): Todo
    {
        $todo = new Todo(
            $user,
            $title,
            $description
        );

        $this->em->persist($todo);
        $this->em->flush();

        return $todo;
    }
}
