<?php

declare(strict_types=1);

namespace App\Request\Base;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseRequest implements DataRequestInterface
{
    public function __construct(
        protected readonly ValidatorInterface $validator,
        protected readonly RequestStack $requestStack,
    ) {
        $this->populate();
        $this->validate();
    }

    public function validate(): void
    {
        $violations = $this->validator->validate($this);

        if ($violations->count() === 0) {
            return;
        }

        $errors = [];
        foreach ($violations as $violation) {
            $errors[] = sprintf('%s : %s', $violation->getPropertyPath(), $violation->getMessage());
        }

        throw new HttpException(400, implode(",", $errors));
    }

    public function getRequest(): Request
    {
        $request = $this->requestStack->getCurrentRequest();

        if (! $request instanceof Request) {
            throw new \LogicException('Current request cannot be null');
        }

        return $request;
    }

    protected function populate(): void
    {
        foreach ($this->getRequest()->toArray() as $property => $value) {
            if (! property_exists($this, $property)) {
                continue;
            }

            $this->{$property} = $value;
        }
    }

    protected function convertJsonBodyToParameterBag(): ParameterBag
    {
        return new ParameterBag($this->getRequest()->toArray());
    }
}
