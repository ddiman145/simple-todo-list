<?php

declare(strict_types=1);

namespace App\Request\Base;

interface DataRequestInterface
{
    /**
     * This should invoke a validation of the Request
     */
    public function validate(): void;

    /**
     * Returns data from the Request. Return changes with each implementation.
     */
    public function getData(): mixed;
}
