<?php

declare(strict_types=1);

namespace App\Request;

use App\Request\Base\BaseRequest;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateUserRequest extends BaseRequest
{
    #[
        Assert\NotBlank(allowNull: false),
        Assert\Type('string')
    ]
    protected string $name;

    #[
        Assert\NotBlank(allowNull: false),
        Assert\Type('string'),
        Assert\Email
    ]
    protected string $username;

    #[
        Assert\NotBlank(allowNull: false),
        Assert\Type('string')
    ]
    protected string $password;


    #[ArrayShape(['name' => "string", 'username' => "string", 'password' => "string"])]
    public function getData(): array
    {
        return [
            'name' => $this->name,
            'username' => $this->username,
            'password' => $this->password
        ];
    }
}
