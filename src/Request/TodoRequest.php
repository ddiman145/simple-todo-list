<?php

declare(strict_types=1);

namespace App\Request;

use App\Request\Base\BaseRequest;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Validator\Constraints as Assert;

final class TodoRequest extends BaseRequest
{
    #[
        Assert\NotBlank(allowNull: false),
        Assert\Type('string')
    ]
    protected string $title;
    #[
        Assert\NotBlank(allowNull: false),
        Assert\Type('string')
    ]
    protected string $description;

    #[ArrayShape(['title' => "string", 'description' => "string"])]
    public function getData(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
        ];
    }
}
