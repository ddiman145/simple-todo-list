<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\ErasedUserDto;
use App\Entity\User;
use App\Facade\UserFacade;
use App\Factory\UserFactory;
use App\Request\CreateUserRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class SecurityController extends AbstractController
{
    public function __construct(
        private readonly UserFactory $userHandler,
        private readonly UserFacade $userFacade,
    ) {
    }

    #[Route('/api/register', name: 'app_user_create', methods: ['POST'])]
    public function create(
        CreateUserRequest $request,
        #[CurrentUser] User $currentUser = null,
    ): Response {
        if ($currentUser !== null) {
            return $this->json([]);
        }

        $data = $request->getData();

        $user = $this->userHandler->create(
            $data['name'],
            $data['username'],
            $data['password']
        );
        $erasedUser = $this->userFacade->getErasedUser($user);

        return $this->json($erasedUser->toArray());
    }

    #[Route('/api/login', name: 'app_login', methods: ['POST'])]
    public function login(#[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
            return $this->json([
                'message' => 'missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $erasedUser = $this->userFacade->getErasedUser($user);

        return $this->json(
            $erasedUser->toArray()
        );
    }
}
