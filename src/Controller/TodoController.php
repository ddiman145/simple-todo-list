<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Todo;
use App\Entity\User;
use App\Factory\TodoFactory;
use App\Handler\TodoHandler;
use App\Repository\TodoRepository;
use App\Request\TodoRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class TodoController extends AbstractController
{
    public function __construct(
        private readonly TodoRepository $todoRepository,
        private readonly TodoFactory $todoFactory,
        private readonly TodoHandler $todoHandler,
    ) {
    }

    #[Route('/api/todo', name: 'app_todo_get', methods: ['GET'])]
    public function index(
        #[CurrentUser] User $user = null,
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->json(
            $this->todoRepository->findBy(['user' => $user]),
            context: ['groups' => 'list']
        );
    }

    #[Route('/api/todo', name: 'app_todo_add', methods: ['POST'])]
    public function add(
        TodoRequest $request,
        #[CurrentUser] User $user = null,
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $data = $request->getData();

        $todo = $this->todoFactory->create(
            $data['title'],
            $data['description'],
            $user ?? throw new UnauthorizedHttpException('Bearer token')
        );

        return $this->getErasedTodo($todo);
    }

    #[Route('/api/todo/{id}', name: 'app_todo_update', methods: ['PUT'])]
    public function update(
        TodoRequest $request,
        Todo $todo = null,
        #[CurrentUser] User $user = null,
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($todo === null) {
            throw new NotFoundHttpException('Todo not found');
        }

        if ($todo->getUser() !== $user) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'User cannot edit this Todo');
        }

        $data = $request->getData();

        $todo = $this->todoHandler->update(
            $todo,
            $data['title'],
            $data['description'],
        );

        return $this->getErasedTodo($todo);
    }

    #[Route('/api/todo/{id}', name: 'app_todo_delete', methods: ['DELETE'])]
    public function delete(
        ?Todo $todo,
        #[CurrentUser] User $user = null,
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($todo === null) {
            throw new NotFoundHttpException('Todo not found');
        }

        if ($todo->getUser() !== $user) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'User cannot delete this Todo');
        }

        $this->todoRepository->remove(
            $todo,
            true
        );

        return $this->json([]);
    }

    private function getErasedTodo(Todo $todo): JsonResponse
    {
        return $this->json(
            $todo,
            context: ['groups' => 'list']
        );
    }
}
