<?php

declare(strict_types=1);

namespace App\DTO;

class ErasedUserDto implements IDto
{
    public function __construct(
        public readonly string $name,
        public readonly string $username,
        public readonly string $token,
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'username' => $this->username,
            'token' => $this->token,
        ];
    }
}
