<?php

declare(strict_types=1);

namespace App\DTO;

class JwtPayload implements IDto
{
    public function __construct(
        public readonly string $sub,
        public readonly int $exp,
    ) {
    }

    public function toArray(): array
    {
        return [
            'sub' => $this->sub,
            'exp' => $this->exp,
        ];
    }
}
