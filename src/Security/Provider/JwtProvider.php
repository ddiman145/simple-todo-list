<?php

declare(strict_types=1);

namespace App\Security\Provider;

use App\DTO\JwtPayload;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class JwtProvider
{
    /**
     * @var string
     */
    private const HASH_ALGORITHM = 'HS256';

    public function __construct(
        private readonly string $jwtSecret,
    ) {
    }

    public function decode(string $token): JwtPayload
    {
        try {
            /** @phpstan-var mixed[] $decodedJwt */
            $decodedJwt = (array) JWT::decode(
                $token,
                new Key($this->jwtSecret, self::HASH_ALGORITHM),
            );
        } catch (SignatureInvalidException) {
            throw new BadCredentialsException('Token cannot be verified', 401);
        }

        if (
            ! array_key_exists('sub', $decodedJwt) ||
            ! array_key_exists('exp', $decodedJwt)
        ) {
            throw new BadCredentialsException('Invalid JWT token');
        }

        return new JwtPayload($decodedJwt['sub'], $decodedJwt['exp']);
    }

    public function encode(JwtPayload $payload): string
    {
        return JWT::encode(
            $payload->toArray(),
            $this->jwtSecret,
            self::HASH_ALGORITHM,
        );
    }
}
