<?php

declare(strict_types=1);

namespace App\Security;

use App\Security\Provider\JwtProvider;
use Symfony\Component\Security\Http\AccessToken\AccessTokenHandlerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;

class AccessTokenHandler implements AccessTokenHandlerInterface
{
    public function __construct(private readonly JwtProvider $jwtProvider)
    {
    }

    public function getUserBadgeFrom(#[\SensitiveParameter] string $accessToken): UserBadge
    {
        $decodedJwt = $this->jwtProvider->decode($accessToken);

        return new UserBadge($decodedJwt->sub);
    }
}
