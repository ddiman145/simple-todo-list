<?php

namespace App\Entity;

use App\Common\Enum\Role;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Orm\Column(type: 'string', nullable: false)]
    private string $password;

    public function __construct(
        #[Orm\Column(type: 'string', nullable: false)]
        private readonly string $name,
        #[Orm\Column(type: 'string', unique: true, nullable: false)]
        private readonly string $username,
        #[Orm\Column(type: 'json', nullable: false)]
        private readonly array $role = [Role::User],
    ) {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): string
    {
        $this->password = $password;

        return $this->password;
    }

    public function getRoles(): array
    {
        return array_unique($this->role);
    }

    public function eraseCredentials(): void
    {
        return;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }
}
