<?php

declare(strict_types=1);

namespace App\Common\Enum;

enum Role: string
{
    case User = 'ROLE_USER';
}
