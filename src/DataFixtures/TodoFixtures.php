<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\TodoFactory;
use App\Repository\TodoRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class TodoFixtures extends Fixture implements DependentFixtureInterface
{
    private readonly Generator $faker;

    public function __construct(
        private readonly TodoFactory $todoFactory,
        private readonly UserRepository $userRepository,
    ) {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        foreach (UserFixtures::DEFINITION as $userDefinition) {
            $user = $this->userRepository->findOneBy(['username' => $userDefinition['username']]);

            if ($user === null) {
                throw new \LogicException(
                    'User not found. Is the id correctly configured or did you run fixtures?',
                );
            }

            $obj = $this->todoFactory->create(
              $this->faker->jobTitle,
              $this->faker->text(),
              $user
            );

            $manager->persist($obj);
            $manager->flush();
        }
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class];
    }
}
