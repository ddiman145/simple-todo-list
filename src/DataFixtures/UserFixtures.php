<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const DEFINITION = [
        [
            'name' => 'Pepa Novak',
            'username' => 'pepa@novak.cz',
            'password' => '123456',
        ],
        [
            'name' => 'Pepa Karel',
            'username' => 'pepa@karel.cz',
            'password' => '123456',
        ],
        [
            'name' => 'Jan Novotny',
            'username' => 'jan@novotny.cz',
            'password' => '123456',
        ],
    ];

    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserFactory $userFactory,
    ) {

    }

    public function load(ObjectManager $manager): void
    {
        $existing = $this->userRepository->findAll();

        foreach (self::DEFINITION as $definition) {
            // Identifier has to be unique. If we already have a row, skip it.
            if (array_filter(
                    $existing,
                    static fn (User $row) => $row->getUsername() === $definition['username'],
                ) !== []) {
                continue;
            }

            $obj = $this->userFactory->create(
              $definition['name'],
              $definition['username'],
              $definition['password'],
            );

            $manager->persist($obj);
            $manager->flush();
        }
    }

}
