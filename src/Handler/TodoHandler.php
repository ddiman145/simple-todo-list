<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Todo;
use Doctrine\ORM\EntityManagerInterface;

class TodoHandler
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function update(Todo $todo, string $title, string $description): Todo
    {
        $todo->setTitle($title)
            ->setDescription($description);

        $this->em->persist($todo);
        $this->em->flush();

        return $todo;
    }
}
